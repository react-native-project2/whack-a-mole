import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, StatusBar} from 'react-native';
import Square from './Square';

const GameBoard = () => {
    let time = 50
  const [timeLeft, setTimeLeft] = useState(time);

  useEffect(() => {
    if (!timeLeft) return

    const timerId = setInterval(() => {
      //happens every 1000 ms
      setTimeLeft(timeLeft - 1);
    }, 1000)
    //clear interval
    return () => clearInterval(timerId)
  }, [timeLeft]);

  return (
    <View style={styles.container}>
      <StatusBar hidden={true} />
      <Text style={styles.title}>Whack-A-Mole Game</Text>
      <Text style={styles.timer}>{timeLeft}</Text>
      <View style={styles.game}>
        <Square time ={time} />
        <Square time ={time}/>
        <Square time ={time}/>
        <Square time ={time}/>
        <Square time ={time}/>
        <Square time ={time}/>
        <Square time ={time}/>
        <Square time ={time}/>
        <Square time ={time}/>
        <Square time ={time}/>
        <Square time ={time}/>
        <Square time ={time}/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'salmon',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff',
  },
  game: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: 350,
    paddingTop: 20,
  },
  timer: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'mistyrose',
  },
});

export default GameBoard;
