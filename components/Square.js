import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';

const Square = ({time}) => {
  const [moleActive, setMoleActive] = useState(false)
  const [isGameOver, setGameOver]  = useState(false)
  // let  timerId = 
  const randomTime = Math.random()  * 20000
  let timerId  

  useEffect (() => {
      timerId = setInterval(() => {
      setMoleActive(true)
      setTimeout(() => {setMoleActive(false)}, 800)
    }, randomTime)
    setTimeout(endGame, time * 1000)
  }, [])

  function endGame () {
    clearInterval(timerId)
    setGameOver(true)
  }

  return <View style={moleActive ? styles.mole : styles.square} />;
};



const styles = StyleSheet.create({
  square: {
    flex: 1,
    minWidth: 80,
    minHeight: 80,
    margin: 10,
    borderRadius: 20,
    backgroundColor: 'mistyrose',
  },
  mole: {
    flex: 1,
    minWidth: 80,
    minHeight: 80,
    margin: 10,
    borderRadius: 20,
    backgroundColor: 'tomato',
  }
});

export default Square;
